Mathesio comments (copy from Nette Sandbox)
=============

Installation
------------
- copy this project to your php RootDirectory
- (on Linux/MAC OS) make directories `temp/` and `log/` writable.
- import sql/import.sql (creates database `mathesio_comments` and tables)
- copy file `app/config/.config.local.neon` to `config.local.neon` and change parameters for your database
- run `composer update`

Run
----------------

You can start the built-in PHP server in the root directory of your project:

	php -S localhost:8000 -t www

Then visit `http://localhost:8000` in your browser to see the welcome page.
