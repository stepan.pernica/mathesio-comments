<?php declare(strict_types=1);

namespace App\Model;

use Nette\Database\Context;
use Nette\Database\Table\ActiveRow;
use Nette\SmartObject;

final class Article
{
	use SmartObject;

	const TABLE_NAME = 'article';

	/** @var Context */
	private $database;



	/**
	 * @param Context $database
	 */
	public function __construct(
		Context $database
	) {
		$this->database = $database;
	}



	/**
	 * @param int $id
	 * @return false|ActiveRow
	 */
	public function getById(int $id)
	{
		return $this->database->table(self::TABLE_NAME)->wherePrimary($id)->fetch();
	}
}
