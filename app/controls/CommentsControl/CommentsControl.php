<?php declare(strict_types=1);

namespace App\Controls;

use App\Model\Comment;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;


interface ICommentsControlFactory
{
	/** @return CommentsControl */
	function create();
}



class CommentsControl extends Control
{
	/** @var int @persistent */
	private $articleId;

	/** @var Comment */
	protected $comment;



	/**
	 * @param Comment $comment
	 */
	public function __construct(
		Comment $comment
	) {
		$this->comment = $comment;
	}



	/**
	 * @return int
	 */
	public function getArticleId(): int
	{
		return $this->articleId;
	}



	/**
	 * @param int $articleId
	 */
	public function setArticleId(int $articleId): void
	{
		$this->articleId = $articleId;
	}



	public function render()
	{
		$template = $this->template;
		$template->articleId = $this->getArticleId();

		$template->comments = $this->comment->getCommentsByArticleId($this->getArticleId());
		$template->setFile(__DIR__ . '/CommentsControl.latte');
		$template->render();
	}



	/**
	 * @return Multiplier
	 */
	protected function createComponentCommentForm()
	{
		return new Multiplier(function ($parentId) {
			$form = new \Nette\Application\UI\Form();
			$form->getElementPrototype()->addAttributes(['class' => 'ajax']);
			$form->addTextArea('text', 'Váš komentář', null, 3)
				->addRule($form::FILLED);
			$form->addHidden('parent_id', $parentId);
			$form->addSubmit('send', 'Odeslat');
			$form->onSuccess[] = [$this, 'addCommentSucceeded'];
			return $form;
		});
	}



	/**
	 * @param $form
	 * @param $values
	 * @throws \Nette\Application\AbortException
	 */
	public function addCommentSucceeded($form, $values)
	{
		$parent_id = $values->parent_id ?? null;
		$this->comment->add($this->getArticleId(), $parent_id, $values->text);
		$form['text']->setValue('');

		if ($this->getPresenter()->isAjax()) {
			$this->redrawControl();
		} else {
			$this->redirect('this');
		}
	}
}
